# docker-nodejs-ssh-public-key

A Docker image ready to run Node.js software. It also accepts SSH connections, authenticating the
user through a private and public key pair.

## Instructions

1. Build the Docker image:

   ```sh
   docker build --rm -t nodejs-ssh-public-key .
   ```

1. Create a public and private key pair:

   ```sh
   ssh-keygen
   ```

1. Start a container providing it the public key as the `SSH_PUBLIC_KEY` environment variable:

   ```sh
   docker run -d -e SSH_PUBLIC_KEY="$(cat ~/.ssh/id_rsa.pub)" --rm nodejs-ssh-public-key
   ```

1. Get container's IP address:

   ```sh
   docker inspect <container-id> | grep '"IPAddress"' | head -n 1
   ```

1. Connect using SSH:

   ```sh
   ssh -i ~/.ssh/id_rsa root@<container-ip>
   ```

1. Verify Node.js version (expected output is `v12.16.1`.):

   ```sh
   node --version
   ```

_**Sidenote**: Replace the public and private key file paths in steps 3 and 5 in case you don't use
`ssh-keygen` defaults._

## References

1. [How to use an entrypoint script to initialize container data at runtime][1]

[1]: https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
